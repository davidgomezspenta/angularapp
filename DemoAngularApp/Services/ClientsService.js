﻿
clientsApp.service("ClientsService", function ($http) {
    this.getClients = function () {
        
        var data = [
        {
            "Id": 1,
            "Name": "Maxam",
            "Year": 2014,
            "ImageUrl": "../Images/orange.jpg",
            "Category": ""
        },
        {
            "Id": 2,
            "Name": "Eurofirms",
            "Year": 2015,
            "ImageUrl": "../Images/eurofirms.PNG",
            "Category": ""
        },
        {
            "Id": 3,
            "Name": "Almirall",
            "Year": 2014,
            "ImageUrl": "../Images/almirall.PNG",
            "Category": "pharmaceutical"
        },
        {
            "Id": 4,
            "Name": "BASF",
            "Year": 2013,
            "ImageUrl": "../Images/basf.png",
            "Category": "chemical"
        },
        {
            "Id": 5,
            "Name": "Abbott",
            "Year": 2014,
            "ImageUrl": "../Images/abbott.jpg",
            "Category": "pharmaceutical"
        },
        {
            "Id": 6,
            "Name": "Bank of America",
            "Year": 2015,
            "ImageUrl": "../Images/bankofamerica.png",
            "Category": "Bank"
        },
        {
            "Id": 7,
            "Name": "Bank of England",
            "Year": 2014,
            "ImageUrl": "../Images/bankofengland.jpg",
            "Category": "Bank"
        },
        {
            "Id": 8,
            "Name": "FAES",
            "Year": 2013,
            "ImageUrl": "../Images/faes.PNG",
            "Category": "pharmaceutical"
        },
        {
            "Id": 9,
            "Name": "Grifols",
            "Year": 2014,
            "ImageUrl": "../Images/grifols.jpg",
            "Category": "pharmaceutical"
        },
        {
            "Id": 10,
            "Name": "Orange",
            "Year": 2013,
            "ImageUrl": "../Images/orange.jpg",
            "Category": "telecommunications"
        },
        {
            "Id": 11,
            "Name": "Telefónica",
            "Year": 2014,
            "ImageUrl": "../Images/Telefonica.jpg",
            "Category": "telecommunications"
        },
        {
            "Id": 12,
            "Name": "Vodafone",
            "Year": 2015,
            "ImageUrl": "../Images/Vodafone.png",
            "Category": "telecommunications"
        },
        {
            "Id": 13,
            "Name": "Yoigo",
            "Year": 2014,
            "ImageUrl": "../Images/Yoigo.jpg",
            "Category": "telecommunications"
        }
        ];
        return data;
    }
});