﻿
clientsApp.controller("ClientsListCtrl", ["$scope","ClientsService", function ($scope,ClientsService) {
    $scope.orderClients = "Name";

    $scope.Categories = ["pharmaceutical", "chemical", "Bank","telecommunications"];

    var clients = ClientsService.getClients();

    $scope.Clients = clients;
    
    $scope.clearFilter = function () {
        $scope.queryClients = "";
        $scope.orderClients = "Name";
        $scope.categoryFilter = undefined;
    }
}]);

clientsApp.controller("ClientDetailCtrl", ['$scope', '$routeParams',
  function ($scope, $routeParams) {
      var clientId = $routeParams.clientIdParam;

      var Clients = [
      {
          "Id": "1",
          "Name": "Maxam",
          "Year": 2014,
          "Projects": [
              {
                  "NameP": "Video Channel",
                  "Description": "Video Channel",
                  "Finished": 1
              }
          ]
      },
      {
          "Id": "2",
          "Name": "Eurofirms",
          "Year": 2015,
          "Projects": [
              {
                  "NameP": "Web Corporativa",
                  "Description": "Web Corporativa",
                  "Finished": 1
              },
              {
                  "NameP": "Areas Privadas",
                  "Description": "Areas Privadas",
                  "Finished": 2
              }
          ]
      },
      {
          "Id": "3",
          "Name": "Almirall",
          "Year": 2014,
          "Projects": [

          ]
      },
      {
          "Id": "4",
          "Name": "BASF",
          "Year": 2013,
          "ImageUrl": "../Images/basf.png",
          "Projects": [
              {
                  "NameP": "Beezy for BASF",
                  "Description": "Beezy for BASF",
                  "Finished": 1
              },
          ]
      }
      ];

      for (var client in Clients) {
          if (Clients[client].Id == clientId) {
              $scope.ClientDetail = Clients[client];
              return;
          }
      }


  }]);