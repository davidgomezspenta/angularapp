﻿var clientsApp = angular.module("clientsApp", ["ngRoute"]);

clientsApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/', {
            templateUrl: 'Views/Clients.html',
            controller: 'ClientsListCtrl'
        }).
        when('/client/:clientIdParam', {
            templateUrl: 'Views/Client-Detail.html',
            controller: 'ClientDetailCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
  }]);