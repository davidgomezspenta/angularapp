﻿clientsApp.directive('clientStyle', function () {
    return {
        restrict: 'AC',
        link: function (scope, element, attrs) {
            var color = "#ddd2d2";
            switch (attrs.category) {
                case "chemical": {
                    color = "#edb280";
                    break;
                }
                case "Bank": {
                    color = "#e8d48a";
                    break;
                }
                case "pharmaceutical": {
                    color = "#b2dede";
                    break;
                }
                case "telecommunications": {
                    color = "#eda9a9";
                    break;
                }
            }
            element.css('background-color', color);
        }
    }
});